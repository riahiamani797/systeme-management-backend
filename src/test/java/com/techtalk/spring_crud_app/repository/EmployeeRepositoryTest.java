/*package com.techtalk.spring_crud_app.repository ;
import com.techtalk.spring_crud_app.model.Employee;
import com.techtalk.spring_crud_app.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE) // This prevents auto-replacement of DataSource
@ExtendWith(SpringExtension.class)
public class EmployeeRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void testGetByName() {
        Employee employee = new Employee();
        employee.setName("John");
        entityManager.persist(employee);
        entityManager.flush();

        Employee found = employeeRepository.getByName("John");

        assertThat(found.getName()).isEqualTo(employee.getName());
    }

    @Test
    //public void testFindByName() {
        Employee employee = new Employee();
        employee.setName("Jane");
        entityManager.persist(employee);
        entityManager.flush();

        Employee found = employeeRepository.findByName("Jane");

        assertThat(found.getName()).isEqualTo(employee.getName());
    }
}*/
