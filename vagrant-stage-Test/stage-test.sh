#!/bin/bash

# Update CentOS and install required packages
sudo yum -y update
sudo yum -y install yum-utils device-mapper-persistent-data lvm2

# Add Docker repository
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# Install Docker
sudo yum -y install docker-ce

# Start and enable Docker service
sudo systemctl start docker
sudo systemctl enable docker

# Add current user to the 'docker' group to use Docker without sudo
sudo usermod -aG docker $USER

echo "Docker installation completed."



# Download the latest version of Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# Make the downloaded binary executable
sudo chmod +x /usr/local/bin/docker-compose

# Create a symbolic link to enable using 'docker-compose' command
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

echo "Docker Compose installation completed."
