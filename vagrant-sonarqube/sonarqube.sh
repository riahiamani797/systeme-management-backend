#!/bin/bash

# Update CentOS and install required packages
sudo yum -y update
sudo yum -y install yum-utils device-mapper-persistent-data lvm2

# Add Docker repository
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# Install Docker
sudo yum -y install docker-ce

# Start and enable Docker service
sudo systemctl start docker
sudo systemctl enable docker

# Add the current user to the "docker" group
sudo usermod -aG docker $USER

# Output Docker version for confirmation
docker --version

echo "Docker installation completed."
